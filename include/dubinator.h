/// @file dubinator.h
#include <string>
#include <map>
/**
 * @brief Dubinator class is the main body of the project
 * 
 */
class Dubinator {
public:
    Dubinator(int arg1, int arg2, std::string operation);
    ~Dubinator() = default;

    int perform_operation();

private:

    int addition();
    int subtraction();
    int multiplication();
    int division();

    enum class operation {
        ADDITION,
        SUBTRACTION,
        MULTIPLICATION,
        DIVISION,
    };
    std::map<std::string, operation> operations = {
        {"addition", operation::ADDITION},
        {"subtraction", operation::SUBTRACTION},
        {"multiplication", operation::MULTIPLICATION},
        {"division", operation::DIVISION}
    };
    int arg1;
    int arg2;
    operation op;
};