#define CATCH_CONFIG_MAIN
#include <catch/catch.hpp>

#include "dubinator.h"

TEST_CASE("Addition of two numbers") {
    Dubinator dub(2, 5, "addition");
    REQUIRE(dub.perform_operation() == 7);
};