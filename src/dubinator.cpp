/// @file dubinator.cpp
#include "dubinator.h"

Dubinator::Dubinator(int arg1, int arg2, std::string operation)
: arg1(arg1), arg2(arg2), op(operations[operation]) {}

int Dubinator::perform_operation() {
    switch (op)
    {
    case operation::ADDITION:
        return addition();

    case operation::SUBTRACTION:
        return subtraction();

    case operation::MULTIPLICATION:
        return multiplication();

    case operation::DIVISION:
        return division();
    
    default:
        return 0;
    }
}

/**
 * @brief Adds the values together
 * 
 * @return int 
 */
int Dubinator::addition(){
    return arg1 + arg2;
}

int Dubinator::subtraction(){
    return arg1 - arg2;
}

int Dubinator::multiplication(){
    return arg1 * arg2;
}

int Dubinator::division(){
    return arg2 ? arg1 / arg2 : arg1;
}