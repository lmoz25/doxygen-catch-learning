#include "dubinator.h"
#include <iostream>

int main(){
    Dubinator dubinator(5, 3, "addition");
    std::cout << "Result: " << dubinator.perform_operation() << std::endl;
}